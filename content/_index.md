## Project Description

OSP - Open Source Patterns will provide a web interface
for visually representing best practices and design patterns
for .NET development.

The overall aim is to educate fellow developers
by accumulating useful guides for software development.

Everyone is welcome to submit their merge requests to improve this service!
Link to repository: https://gitlab.com/simsonsjanis/OSP/


